import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import database from '@react-native-firebase/database';

let usersRef = database().ref('/users');
var newUser = {
  userName: 'test',
  email: 'test@gmail.com',
  password: '123456',
  project: [
    {
      name: 'RN',
    },
  ],
};

const LoginScreen = () => {
  const [valueFour, setValueOne] = useState('');
  const [valueFive, setValueTwo] = useState('');

  const [data, setData] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    usersRef.on('value', snapshot => {
      let datas = snapshot.val();
      if (datas != null && datas != undefined) {
        const users = Object.values(datas);
        setData(users);
      }
    });
  }, []);

  function checkLogin() {
    var verfiedUser = data.find(x => x.email == valueFour);

    if (valueFour == '' || valueFive == '') {
      Alert.alert('Please Enter all the fields');
    } else if (
      verfiedUser != undefined &&
      verfiedUser.email == valueFour &&
      verfiedUser.password == valueFive
    ) {
      var verifiedUserName = verfiedUser.userName;
      console.log('userName', verifiedUserName);
      setValueOne('');
      setValueTwo('');
      navigation.navigate('Home', {valueFour, verifiedUserName});
    } else {
      Alert.alert('Invalid login');
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 120,
        }}>
        <Text style={{padding: 30, fontWeight: '700', fontSize: 20}}>
          LOGIN
        </Text>
        <TextInput
          placeholder={'Email'}
          value={valueFour}
          style={styles.textInput}
          onChangeText={text => setValueOne(text)}
        />
        <TextInput
          placeholder={'Password'}
          value={valueFive}
          style={styles.textInput}
          secureTextEntry={true}
          onChangeText={text => setValueTwo(text)}
        />
        <TouchableOpacity style={styles.addBtn} onPress={() => checkLogin()}>
          <Text style={styles.addText}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.addBtn}
          onPress={() => navigation.navigate('Register')}>
          <Text style={styles.addText}>Register</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  addBtn: {
    margin: 10,
    width: '75%',
    height: 45,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '75%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
  },
  textStyle: {
    margin: 10,
  },
});

export default LoginScreen;
