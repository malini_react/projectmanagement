import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import database from '@react-native-firebase/database';

let usersRef = database().ref('/users');

const Project = ({route}) => {
  const {valueFour} = route.params;
  const {verifiedUserName} = route.params;

  const [click, setClick] = useState(false);
  const [valueOne, setValueOne] = useState('');
  const [valueTwo, setValueTwo] = useState('');
  const [dummy, setDummy] = useState('dummy');

  const [data, setData] = useState([]);
  const [response, setResponse] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    console.log('use Effect called');
    usersRef.on('value', snapshot => {
      let datas = snapshot.val();
      if (datas != null && datas != undefined) {
        const users = Object.values(datas);
        users.map(x => {
          if (x.email == valueFour) {
            console.log('project', x.project);
            setResponse(x.project);
          }
        });
      }
    });
  }, []);

  function showTextInput() {
    setClick(true);
  }

  function saveProjectData() {
    if (valueOne != '' && valueTwo != '') {
      setData([
        ...data,
        {id: valueOne, name: valueTwo, createdBy: verifiedUserName, tasks: []},
      ]);
      setClick(false);
      setValueOne('');
      setValueTwo('');
      database()
        .ref('users')
        .once('value')
        .then(function (snapshot) {
          let datas = snapshot.val();
          for (var key in datas) {
            if (datas[key].email == valueFour) {
              var db = database();
              var ref = db.ref();
              var usersRef1 = ref.child('users');
              // alanisawesome is the key of the object
              var hopperRef = usersRef1.child(key);
              hopperRef.update({
                project: [...data],
              });
              // database().ref('users').child(key).update(datas[key]);
            }
          }
        });
      Alert.alert('Project created successfully ');
    } else {
      Alert.alert('Please enter all the fields');
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <View>
        {response != null && response.length > 0 ? (
          <FlatList
            data={response}
            keyExtractor={item => item.id}
            renderItem={({item}) => (
              <View
                style={{
                  padding: 10,
                  backgroundColor: 'white',
                  margin: 10,
                  borderRadius: 5,
                }}>
                <TouchableOpacity
                  style={styles.touchableOpacityStyle}
                  onPress={() => {
                    navigation.navigate('Tasks', {item, valueFour});
                  }}>
                  <View style={{marginTop: 10, marginBottom: 10}}>
                    <Text style={styles.textStyle}>
                      Project name : {item.name}
                    </Text>
                    <Text style={styles.textStyle}>
                      Created by : {item.createdBy}
                    </Text>
                    <Text style={styles.textStyle}>
                      Created on : Nov 15, 2021
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          />
        ) : null}
      </View>
      {click ? (
        <View style={{backgroundColor: 'white'}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginLeft: 10,
            }}>
            <Text style={{alignSelf: 'center', width: '35%'}}>Project Id</Text>
            <TextInput
              value={valueOne}
              style={styles.textInput}
              onChangeText={text => setValueOne(text)}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginLeft: 10,
            }}>
            <Text style={{alignSelf: 'center', width: '35%'}}>
              Project name
            </Text>
            <TextInput
              value={valueTwo}
              style={styles.textInput}
              onChangeText={text => setValueTwo(text)}
            />
          </View>
          <TouchableOpacity
            style={styles.textInputAddBtn}
            onPress={() => saveProjectData()}>
            <Text style={styles.addText}>Save</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <TouchableOpacity style={styles.addBtn} onPress={() => showTextInput()}>
          <Text style={[styles.addText, {fontWeight: '700', fontSize: 20}]}>
            {' '}
            +{' '}
          </Text>
        </TouchableOpacity>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D3D3D3',
  },
  addBtn: {
    margin: 10,
    width: '10%',
    height: 40,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
    borderRadius: 100,
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    justifyContent: 'center',
  },
  textStyle: {
    margin: 2,
  },
});

export default Project;
