import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
  Modal,
} from 'react-native';
import database from '@react-native-firebase/database';
import {useNavigation} from '@react-navigation/native';

let usersRef = database().ref('/users');

const ProjectDetails = ({route}) => {
  const [click, setClick] = useState(false);
  const [valueOne, setValueOne] = useState('');
  const [valueTwo, setValueTwo] = useState('');
  const [valueThree, setValueThree] = useState('');
  const [valueSix, setValueSix] = useState('');
  const [valueFive, setValueFive] = useState('');
  const [data, setData] = useState([]);
  const [response, setResponse] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [valueSeven, setValueSeven] = useState('');
  const [valueEight, setValueEight] = useState('');
  const [team, setTeam] = useState([]);

  const {item} = route.params;
  var projectName = item;
  const {valueFour} = route.params;

  const navigation = useNavigation();

  useEffect(() => {
    usersRef.on('value', snapshot => {
      let datas = snapshot.val();
      if (datas != null && datas != undefined) {
        const users = Object.values(datas);
        users.map(x => {
          if (x.email == valueFour) {
            x.project.map(y => {
              if (y.name == item.name) {
                setResponse(y.task);
              }
            });
          }
        });
      }
    });
  }, []);

  function showTextInput() {
    setClick(true);
  }

  function saveTaskData() {
    if (
      valueOne != '' &&
      valueTwo != '' &&
      valueThree != '' &&
      valueSix != '' &&
      valueFive != ''
    ) {
      setData([
        ...data,
        {
          id: valueOne,
          name: valueTwo,
          description: valueThree,
          startDate: valueSix,
          endDate: valueFive,
          teamMembers: team,
        },
      ]);
      // item.tasks.push({
      //   id: valueOne,
      //   name: valueTwo,
      //   description: valueThree,
      //   startDate: valueSix,
      //   endDate: valueFive,
      // });
      // console.log('tasks', item.tasks);
      // setData(item);
      // console.log('newItem', data.tasks);

      setClick(false);
      setValueOne('');
      setValueTwo('');
      setValueThree('');
      setValueSix('');
      setValueFive('');
      database()
        .ref('users')
        .once('value')
        .then(function (snapshot) {
          let datas = snapshot.val();
          for (var key in datas) {
            if (datas[key].email == valueFour) {
              var taskDatas = datas[key].project;
              for (var keys in taskDatas) {
                if (taskDatas[keys].name == item.name) {
                  console.log('task same ');
                  var db = database();
                  var ref = db.ref();
                  var usersRef1 = ref.child('users');
                  // alanisawesome is the key of the object
                  var hopperRef = usersRef1.child(key);
                  var projectRef = hopperRef.child('project');
                  var projectRef1 = projectRef.child(keys);

                  projectRef1.update({
                    task: [...data],
                  });
                  // database().ref('users').child(key).update(datas[key]);
                }
              }
            }
          }
        });
      Alert.alert('Tasks created successfully ');
    } else {
      Alert.alert('Please enter all the fields');
    }
  }

  function saveTeamMembers() {
    setValueSeven('');
    setValueEight('');
    setTeam([...team, {name: valueSeven, pay: valueEight}]);
    setModalVisible(false);
  }

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <Text style={styles.addText}>Project : {item.name}</Text>
        {response != null && response.length > 0 ? (
          <FlatList
            data={response}
            keyExtractor={items => items.id}
            renderItem={({item}) => (
              <TouchableOpacity
                style={styles.touchableOpacityStyle}
                onPress={() =>
                  navigation.navigate('Task', {item, projectName, valueFour})
                }>
                <View>
                  <Text style={styles.textStyle}>{item.name}</Text>
                </View>
              </TouchableOpacity>
            )}
          />
        ) : null}
      </View>
      <Modal
        animationType="slide"
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginLeft: 10,
              }}>
              <Text style={{alignSelf: 'center', width: '35%'}}>
                Team member name
              </Text>
              <TextInput
                value={valueSeven}
                style={styles.textInput}
                onChangeText={text => setValueSeven(text)}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginLeft: 10,
              }}>
              <Text style={{alignSelf: 'center', width: '35%'}}>Pay/hour</Text>
              <TextInput
                value={valueEight}
                style={styles.textInput}
                onChangeText={text => setValueEight(text)}
              />
            </View>
            <TouchableOpacity
              style={styles.addBtn}
              onPress={() => saveTeamMembers()}>
              <Text style={[styles.addText, {color: 'white'}]}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      {click ? (
        <View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginLeft: 10,
            }}>
            <Text style={{alignSelf: 'center', width: '35%'}}>Task Id</Text>
            <TextInput
              value={valueOne}
              style={styles.textInput}
              onChangeText={text => setValueOne(text)}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginLeft: 10,
            }}>
            <Text style={{alignSelf: 'center', width: '35%'}}>Task name</Text>
            <TextInput
              value={valueTwo}
              style={styles.textInput}
              onChangeText={text => setValueTwo(text)}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginLeft: 10,
            }}>
            <Text style={{alignSelf: 'center', width: '35%'}}>
              Task description
            </Text>
            <TextInput
              value={valueThree}
              style={styles.textInput}
              onChangeText={text => setValueThree(text)}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginLeft: 10,
            }}>
            <Text style={{alignSelf: 'center', width: '35%'}}>
              Task start date
            </Text>
            <TextInput
              value={valueSix}
              style={styles.textInput}
              onChangeText={text => setValueSix(text)}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginLeft: 10,
            }}>
            <Text style={{alignSelf: 'center', width: '35%'}}>
              Task end date
            </Text>
            <TextInput
              value={valueFive}
              style={styles.textInput}
              onChangeText={text => setValueFive(text)}
            />
          </View>
          <TouchableOpacity
            style={styles.addBtn}
            onPress={() => setModalVisible(true)}>
            <Text style={[styles.addText, {color: 'white'}]}>
              Add team members
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.textInputAddBtn}
            onPress={() => saveTaskData()}>
            <Text style={[styles.addText, {color: 'white'}]}>Save</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <TouchableOpacity
          style={{
            margin: 10,
            width: '10%',
            height: 40,
            backgroundColor: 'green',
            justifyContent: 'center',
            alignItems: 'flex-end',
            alignSelf: 'flex-end',
            borderRadius: 100,
          }}
          onPress={() => showTextInput()}>
          <Text
            style={[
              styles.addText,
              {color: 'white', fontWeight: '700', fontSize: 20},
            ]}>
            {' '}
            +{' '}
          </Text>
        </TouchableOpacity>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 45,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  addText: {
    alignSelf: 'center',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
  },
  textStyle: {
    margin: 10,
  },
});

export default ProjectDetails;
