import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Project from './Project';
import ProjectDetails from './ProjectDetails';
import LoginScreen from './LoginScreen';
import RegisterScreen from './Register';
import TaskDetails from './TaskDetails';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={RegisterScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen name="Home" component={Project} />
        <Stack.Screen name="Tasks" component={ProjectDetails} />
        <Stack.Screen name="Task" component={TaskDetails} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
