import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import database from '@react-native-firebase/database';

const RegisterScreen = () => {
  const [valueOne, setValueOne] = useState('');
  const [valueTwo, setValueTwo] = useState('');
  const [valueThree, setValueThree] = useState('');

  const [data, setData] = useState([]);

  const navigation = useNavigation();

  function registerUser() {
    database().ref('/users').push({
      userName: valueThree,
      email: valueOne,
      password: valueTwo,
    });
    navigation.navigate('Login');
  }

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 120,
        }}>
        <Text style={{padding: 30, fontWeight: '700', fontSize: 20}}>
          REGISTER
        </Text>

        <TextInput
          placeholder={'User name'}
          value={valueThree}
          style={styles.textInput}
          onChangeText={text => setValueThree(text)}
        />
        <TextInput
          placeholder={'Email'}
          value={valueOne}
          style={styles.textInput}
          onChangeText={text => setValueOne(text)}
        />
        <TextInput
          placeholder={'Password'}
          value={valueTwo}
          style={styles.textInput}
          secureTextEntry={true}
          onChangeText={text => setValueTwo(text)}
        />
        <TouchableOpacity style={styles.addBtn} onPress={() => registerUser()}>
          <Text style={styles.addText}>Register</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  addBtn: {
    margin: 10,
    width: '75%',
    height: 45,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '75%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
  },
  textStyle: {
    margin: 10,
  },
});

export default RegisterScreen;
