import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
  Modal,
} from 'react-native';
import database from '@react-native-firebase/database';
import {useNavigation} from '@react-navigation/native';

let usersRef = database().ref('/users');

const TaskDetails = ({route}) => {
  const {item} = route.params;
  const {projectName} = route.params;
  const {valueFour} = route.params;

  console.log('task', item);

  const navigation = useNavigation();

  const [modalVisible, setModalVisible] = useState(false);
  const [valueNine, setValueNine] = useState('');

  //   useEffect(() => {
  //     usersRef.on('value', snapshot => {
  //       let datas = snapshot.val();
  //       if (datas != null && datas != undefined) {
  //         const users = Object.values(datas);
  //         users.map(x => {
  //           if (x.email == valueFour) {
  //             x.project.map(y => {
  //               if (y.name == projectName.name) {

  //                 setResponse(y.task);
  //               }
  //             });
  //           }
  //         });
  //       }
  //     });
  //   }, []);

  function saveData() {
    setValueNine('');
    database()
      .ref('users')
      .once('value')
      .then(function (snapshot) {
        let datas = snapshot.val();
        for (var key in datas) {
          if (datas[key].email == valueFour) {
            var taskDatas = datas[key].project;
            for (var keys in taskDatas) {
              if (taskDatas[keys].name == projectName.name) {
                var taskDatas1 = taskDatas[keys].task;
                for (var keyss in taskDatas1) {
                  if (taskDatas1[keyss].name == item.name) {
                    console.log('task same ');
                    var db = database();
                    var ref = db.ref();
                    var usersRef1 = ref.child('users');
                    var hopperRef = usersRef1.child(key);
                    var projectRef = hopperRef.child('project');
                    var projectRef1 = projectRef.child(keys);
                    var taskRef = projectRef1.child('task');
                    var taskRef1 = taskRef.child(keyss);

                    taskRef1.update({
                      completed: true,
                      hoursWorked: valueNine,
                    });
                  }
                }

                // database().ref('users').child(key).update(datas[key]);
              }
            }
          }
        }
      });
    setModalVisible(false);
    navigation.goBack();
  }
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
            padding: 20,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Project </Text>
          <Text style={{alignSelf: 'center', width: '45%'}}>
            {projectName.name}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
            padding: 20,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Task </Text>
          <Text style={{alignSelf: 'center', width: '45%'}}>{item.name}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
            padding: 20,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Task desc </Text>
          <Text style={{alignSelf: 'center', width: '45%'}}>
            {item.description}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
            padding: 20,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Duration </Text>
          <Text style={{alignSelf: 'center', width: '45%'}}>
            {item.startDate} - {item.endDate}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
            padding: 20,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Team </Text>
          {item.teamMembers.map(x => {
            return (
              <View style={{flexDirection: 'column'}}>
                <Text style={{alignSelf: 'center', width: '75%'}}>
                  {x.name} ({x.pay}/hr)
                </Text>
              </View>
            );
          })}
        </View>
        {item.completed == undefined ? (
          <TouchableOpacity
            style={styles.addBtn}
            onPress={() => setModalVisible(true)}>
            <Text style={[styles.addText, {color: 'white'}]}>Completed</Text>
          </TouchableOpacity>
        ) : (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginLeft: 10,
              padding: 20,
            }}>
            <Text style={{alignSelf: 'center', width: '35%'}}>Status </Text>
            <Text style={{alignSelf: 'center', width: '45%'}}>Completed</Text>
          </View>
        )}
      </View>
      <Modal
        animationType="slide"
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginLeft: 10,
              }}>
              <Text style={{alignSelf: 'center', width: '35%'}}>
                Number of hours worked
              </Text>
              <TextInput
                value={valueNine}
                style={styles.textInput}
                onChangeText={text => setValueNine(text)}
              />
            </View>
            <TouchableOpacity style={styles.addBtn} onPress={() => saveData()}>
              <Text style={[styles.addText, {color: 'white'}]}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 45,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  addText: {
    alignSelf: 'center',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
  },
  textStyle: {
    margin: 10,
  },
});

export default TaskDetails;
